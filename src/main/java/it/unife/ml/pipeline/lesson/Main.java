/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.pipeline.lesson;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println(addizione(2, 2));
        
    }
    
    public static int addizione(int a, int b) {
        return a + b;
    }
    
    public static int sottrazione(int a, int b) {
        return a - b;
    }
    
}

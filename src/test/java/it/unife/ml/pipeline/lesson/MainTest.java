/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unife.ml.pipeline.lesson;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Giuseppe Cota <giuseppe.cota@unife.it>
 */
public class MainTest {
    
    public MainTest() {
    }

    
    /**
     * Test of addizione method, of class Main.
     */
    @Test
    public void testAddizione() {
        System.out.println("addizione");
        // argomenti di input
        int a = 4;
        int b = 5;
        // valore atteso
        int expResult = 9;
        // in result c'è il valore reale
        int result = Main.addizione(a, b);
        // confronto se in valore atteso è uguale a quello reale
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSottrazione() {
        System.out.println("sottrazione");
        // argomenti di input
        int a = 6;
        int b = 5;
        // valore atteso
        int expResult = 1;
        // in result c'è il valore reale
        int result = Main.sottrazione(a, b);
        // confronto se in valore atteso è uguale a quello reale
        assertEquals(expResult, result);
    }
    
}
